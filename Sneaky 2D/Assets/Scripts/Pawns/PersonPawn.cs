﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonPawn : Pawn
{
    public GameObject Victory;  //used for holding the victory condition
    public Vector3 RespawnPoint;  //used to hold the last respawn point
    public GameManager GM;       //Veriables for varying things


    // Use this for initialization
    public override void Start()
	{
		base.Start();
        Victory.gameObject.SetActive(false);  //turns off the victory object 
        RespawnPoint = transform.position;  //sets the base spwan as the frist respawn point


    }

    // Update is called once per frame
    public override void Update()
	{
		base.Update();  //controller and pawn overriding functions
	}

	public override void MoveForward()  //overriding the base controls functions, same for the rest 
	{
		tf.transform.Translate(Time.deltaTime * speed, 0, 0);
	}

	public override void MoveBackward()
	{
		tf.transform.Translate(-Time.deltaTime * speed, 0, 0);
	}

	public override void RotateLeft()
	{
		tf.transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);
	}

	public override void RotateRight()
	{
		tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);
	}

	public override void GoHome()
	{
		tf.transform.position = homePosition;
	}

    void OnTriggerEnter2D(Collider2D other)  //you win if you colide with target obj
    {
        if (other.tag == "Winning") //Checking for win condition
        {
            Victory.gameObject.SetActive(true);
            Time.timeScale = 0;
        }

        if (other.tag == "Enemy") //Death and respawning, calls the death function for the player and whatnot
        {
            GM.Respawn();
        }
    }
}