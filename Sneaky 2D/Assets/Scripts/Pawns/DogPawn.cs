﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogPawn : Pawn
{
    /// <summary>
    /// This isnt used. I wanted to use it for a different idea but ended up not using it
    /// </summary>


	// Use this for initialization
	public override void Start()  //overiding base class stuff
	{
		base.Start();
	}

	// Update is called once per frame
	public override void Update()
	{
		base.Update();
	}

	public override void MoveForward()
	{
		tf.transform.Translate(Time.deltaTime * speed, 0, 0);
	}

	public override void MoveBackward()
	{
		tf.transform.Translate(-Time.deltaTime * speed, 0, 0);
	}

	public override void Roll()
	{
		tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);
	}

	public override void GoHome()
	{
		tf.transform.position = homePosition;
	}
}