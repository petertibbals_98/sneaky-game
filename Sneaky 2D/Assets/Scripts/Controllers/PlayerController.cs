﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used for the player as the framework and used to be called on
/// </summary>

// Inherit from Controller
public class PlayerController : Controller
{
    public bool makingNoise = false;  //Used to check if making noise


    // Use this for initialization
    public override void Start()
	{
		base.Start();
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
		{
			pawn.MoveForward();
            makingNoise = true;
        }
		if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
		{
			pawn.MoveBackward();
            makingNoise = true;
        }
		if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
		{
			pawn.RotateLeft();
        }
		if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
		{
			pawn.RotateRight();
        }
		if (Input.GetKey(KeyCode.H))
		{
			pawn.GoHome();

		}
		if (Input.GetKey(KeyCode.R))
		{
			pawn.Roll();
		}
	}
}