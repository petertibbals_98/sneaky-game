﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AHH : MonoBehaviour {

    /// <summary>
    /// I used this to try and mess around with the ai but a lot of it didnt work so I scrapped it. 
    /// </summary>

    public bool isIdle = true;  //Bools used to move between states
    public bool isChasing = false;
    private Transform target;
    public float speed;  //its speed

    //Raycast 
    private RaycastHit2D rayCastHit; //raycasting
    private const float RAYCASTDIST = 5f; //good coding practice to not hard code values

    //Idle
    public virtual IEnumerator Idle()
    {
        Vector2 dir = this.transform.TransformDirection(Vector2.left) * RAYCASTDIST; //used for debug draw ray cast

        while (isIdle)
        {
            
            rayCastHit = Physics2D.Raycast(this.transform.position, Vector2.left, RAYCASTDIST); //fires the ray from the transform position to the left for a distance return the hit object
            Debug.DrawRay(this.transform.position, dir, Color.red); //debug statement to see the ray to test
            if(rayCastHit.collider != null && rayCastHit.collider.gameObject.name == "Player")   //rememeber not the best way to search for gameobjects but good for testing. Check out layers in unity for better sorting.
            {
                Debug.Log(this.gameObject.name + " spotted the player!");

            }
            else
            {
                Debug.Log(this.gameObject.name + " is searching.");
            }

            yield return null;

        }

        yield return null;
    }

    //Chase State
    public virtual IEnumerator Chase(GameObject go)  //state for chasing after the player
    {
        isIdle = false;
        isChasing = true;
        Debug.Log(this.gameObject.name + " is chasing " + go.gameObject.name);
        while (isChasing)
        {
            Debug.Log("HEY!");
            if (Vector2.Distance(transform.position, target.position)> 3)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            }
            yield return null;
        }
        yield return null;
    }


    void Awake()
    {
        StartCoroutine(Idle());
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update()
    {

    }

    //as soon as a collider enters the collider attached to this game object this method is called once per collider
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(this.gameObject.name + " spotted " + other.gameObject.name);
        if (other.gameObject.name == "Player")
            StartCoroutine(Chase(other.gameObject));
    }

    //this is called when a collider leaves the collider set as a trigger
    void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log(this.gameObject.name + " is searching.");
    }

    //this is called when a collider sits in the trigger
    /*void OnTriggerStay2D(Collider2D other)
    {
        Debug.Log(this.gameObject.name + " chasing " + other.gameObject.name);
    }*/


}
