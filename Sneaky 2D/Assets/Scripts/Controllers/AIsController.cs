﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIsController : MonoBehaviour
{
    /// <summary>
    /// This is a for the ai. I am pretty sure this is a FSM. It uses funations based on things it sees. 
    /// I just had issues making it do what I wanted it to do but the debugs help show that it still sees tha responds to the player
    /// </summary>

    public enum StateType
    {
        patrol, chase
    }

    public Transform pointA;
    public Transform pointB;
    public bool atPointA;
    public bool atPointB;
    public StateType currState;
    public PlayerController player;
    public float detectionRange;
    public float fov;
    public GameObject alert;

    public Transform tf;
    private SpriteRenderer mySpriteRenderer;
    public float speed;
    private Transform target;

    // Use this for initialization
    void Start()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        tf = GetComponent<Transform>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
        // if statements to switch between states
        if (currState == StateType.patrol)
        {
            patrol();
        }
        else if (currState == StateType.chase)
        {
            chase();
        }
       // Debug.Log("see the player");
        if (CanSeePlayer())
        {
            alert.SetActive(true);
            currState = StateType.chase;
        }
        else
        {
            currState = StateType.patrol;
            alert.SetActive(false);
        }
    }

    void patrol()
    {
        // patrol function, go from one point and back repeatedly
        Vector3 dir;
        if (atPointA)
        {
            dir = -transform.position + pointA.position;
            mySpriteRenderer.flipX = false; //Fliping sprite
        }
        else
        {
            dir = -transform.position + pointB.position;
            mySpriteRenderer.flipX = true;

        }
        if (dir.magnitude < 0.5f)
        {
             atPointA = !atPointA;
        }
        move(dir);
    }

    void chase()
    {
        // chase function, following after the player
        Vector3 dir;
        dir = -transform.position + player.transform.position;
        move(dir);
    }

    bool CanSeePlayer()
    {
        // Detection range using raycast
        Vector3 rayDirection = player.transform.position - transform.position;
        RaycastHit2D hit2d = Physics2D.Raycast(transform.position, rayDirection);
        Debug.Log("check to see player");
        if (player.makingNoise)
        {
            Debug.Log("player making noise");
            Debug.DrawRay(transform.position, rayDirection, Color.red);
            //if (Physics2D.Raycast(transform.position, rayDirection, out hit))
            if(hit2d.collider != null)
            { // Detect player if they are behind 
                Debug.Log("raycast do things");
                if ((hit2d.collider.gameObject.GetComponent<PlayerController>()) && (rayDirection.magnitude <= detectionRange))
                {
                    Debug.Log("can see player");
                    return true;
                }
            }
        }

        if ((Vector3.Angle(rayDirection, transform.forward)) < fov)
        { // Detect if player is within the field of view
            Debug.Log("player in view");
            if (hit2d.collider != null)
            {
                Debug.Log("Working");
                if (hit2d.collider.transform.GetComponent<PlayerController>())
                {
                    Debug.Log("Can see player");
                    chase();
                    tf.position = Vector2.MoveTowards(tf.position, target.position, speed * Time.deltaTime);
                    return true;
                }
                else
                {
                    Debug.Log("Can not see player");
                    return false;
                }
            }
        }
        return false;
    }

    public void move(Vector3 dir) //Moving code
    {
        dir.y = 0;
        dir.Normalize();
        Debug.Log(dir);
        tf.position += dir * speed * Time.deltaTime;
    }
}
