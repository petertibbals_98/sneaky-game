﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void PlayGame()  //Code to move to next scene when the button is clicked
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //Code to quit game when the quit button is clicked
    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

}
