﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    /// <summary>
    /// Really sure this isnt used in the project
    /// </summary>
    

    Transform tf;
    public float speed;
    public float rotationSpeed;

    // Use this for initialization
    void Start ()
    {
        tf = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Basic movement with arrows or WASD
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            tf.transform.Translate(Time.deltaTime * speed, 0, 0);
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            tf.transform.Translate(-Time.deltaTime * speed, 0, 0);
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            tf.transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);
        }


    }
}
