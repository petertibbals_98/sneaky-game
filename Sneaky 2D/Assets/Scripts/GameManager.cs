﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject currentPlayer;

    public List<GameObject> players;

    public GameObject Heart_1;  //Holds the hearts for the UI
    public GameObject Heart_2;  //Holds the hearts for the UI
    public GameObject Heart_3;  //Holds the hearts for the UI
    public GameObject GameOver; //Holds the GameOver for the UI
    public GameObject Victory;  //Holds the victory for the UI
    public static int Health;   //Int used for the players health

    public float respawnDelay;  //how long the player waits to respawn
    public PersonPawn gamePlayer;

    private void Awake()
    {
        gamePlayer = FindObjectOfType<PersonPawn>();

        // Setup the singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        //Setting health
        Health = 3;
        Heart_1.gameObject.SetActive(true);
        Heart_2.gameObject.SetActive(true);
        Heart_3.gameObject.SetActive(true);
        GameOver.gameObject.SetActive(false);
        //Victory.gameObject.SetActive(false);

        players[0].SetActive(true);
        players[1].SetActive(false);
        currentPlayer = players[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (Health > 3) //Making sure that you cant break things

            Health = 3;

        switch (Health)  //Setting the health based on turning things on and off
        {                //Sets the health shown based on the health of the player
            case 3:
                Heart_1.gameObject.SetActive(true);
                Heart_2.gameObject.SetActive(true);
                Heart_3.gameObject.SetActive(true);
                break;

            case 2:
                Heart_1.gameObject.SetActive(true);
                Heart_2.gameObject.SetActive(true);
                Heart_3.gameObject.SetActive(false);
                break;

            case 1:
                Heart_1.gameObject.SetActive(true);
                Heart_2.gameObject.SetActive(false);
                Heart_3.gameObject.SetActive(false);
                break;

            case 0: ///Prob a way better way to do this!!
                Heart_1.gameObject.SetActive(false);
                Heart_2.gameObject.SetActive(false);
                Heart_3.gameObject.SetActive(false);
                Time.timeScale = 0;
                GameOver.gameObject.SetActive(true);
                break;
        }

        if (Input.GetKeyDown(KeyCode.P)) //Swapping pawns and stuff
        {
            if (players[0].activeInHierarchy)
            {
                players[0].SetActive(false);
                players[1].SetActive(true);
                currentPlayer = players[1];
            }
            else
            {
                players[0].SetActive(true);
                players[1].SetActive(false);
                currentPlayer = players[0];

            }
        }
    }

    public void Respawn()
    {
        StartCoroutine("RespawnCoroutine");  //used to make the playe wait before they are respawned, quaility of life kinda thing.
    }

    public IEnumerator RespawnCoroutine() //Respawning countdown
    {
        gamePlayer.gameObject.SetActive(false);
        yield return new WaitForSeconds(respawnDelay);
        gamePlayer.transform.position = gamePlayer.RespawnPoint;
        gamePlayer.gameObject.SetActive(true);
        GameManager.Health -= 1;
    }
}
